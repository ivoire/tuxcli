import json
from pathlib import Path
import time

import tuxcli.colors as colors
import tuxcli.icons as icons
from tuxcli.models import Build
from tuxcli.requests import apiurl, get
from tuxcli.utils import datediff

import requests
import subprocess


def handle_config(options, config):
    ret = get(
        config,
        f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
    )
    if ret.status_code != 200:
        raise NotImplementedError()

    # TODO: add a wrapper around requests.get
    ret = requests.get(f"{ret.json()['download_url']}config")
    if ret.status_code != 200:
        raise NotImplementedError()
    print(ret.text)
    return 0


def handle_download(options, config):
    ret = get(
        config,
        f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
    )
    if ret.status_code != 200:
        raise NotImplementedError()

    # TODO: add a wrapper around requests.get
    download_url = ret.json()["download_url"]
    ret = requests.get(f"{download_url}metadata.json")
    if ret.status_code != 200:
        raise NotImplementedError()
    data = ret.json()

    # create the output directory
    options.output.mkdir(parents=True, exist_ok=True)
    filename = options.output / "metadata.json"
    print(f"{download_url}metadata.json => {filename}")
    with filename.open("wb") as f:
        for buff in ret.iter_content(32768):
            f.write(buff)
    # download artifacts one by one
    for (k, v) in data.get("results", {}).get("artifacts", {}).items():
        if options.only and k not in options.only:
            continue
        for artifact in v:
            url = f"{download_url}{artifact}"
            filename = options.output / artifact
            print(f"{url} => {filename}")
            ret = requests.get(url, stream=True)
            if ret.status_code != 200:
                raise NotImplementedError()
            with filename.open("wb") as f:
                for buff in ret.iter_content(32768):
                    f.write(buff)
    return 0


def handle_get(options, config):
    ret = get(
        config,
        f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
    )
    if ret.status_code != 200:
        raise NotImplementedError()

    build = Build.new(**ret.json())
    if options.json:
        print(build.as_json())
    else:
        print(f"url      : {apiurl(config, build.url())}")
        print(f"download : {build.download_url}")
        print(f"project  : {build.project}")
        print(f"uid      : {build.uid}")
        print(f"plan     : {build.plan}")
        if build.waited_by:
            print(f"tests    : {', '.join(build.waited_by)}")
        print(f"user     : {build.user}")

        print(f"kconfig  : {', '.join(build.kconfig)}")
        print(f"target   : {build.target_arch}@{build.toolchain}")
        print(f"git repo : {build.git_repo}")
        print(f"git ref  : {build.git_ref}")
        print(f"git sha  : {build.git_sha}")
        print(f"git desc : {build.git_describe}")

        if build.provisioning_time:
            print(f"{icons.PROVISIONING} time  : {build.provisioning_time}")
        if build.running_time:
            print(f"{icons.RUNNING} time  : {build.running_time}")

        if build.state == "finished":
            if build.result == "pass" and build.warnings_count == 0:
                icon = icons.PASS
            elif build.result == "pass" and build.warnings_count != 0:
                icon = icons.WARNING
            elif build.result == "error":
                icon = icons.ERROR
            elif build.result == "fail":
                icon = icons.FAIL
            print(f"{icon} time  : {build.finished_time}")
        if build.duration:
            print(f"duration : {build.duration}")

        print(f"state    : {build.state}")
        color = ""
        if build.result == "pass":
            color = colors.green
        elif build.result in ["error", "fail"]:
            color = colors.red
        print(f"result   : {color}{build.result}{colors.reset}")

        if build.errors_count:
            print(f"warnings : {colors.red}{build.errors_count}{colors.reset}")
        if build.warnings_count:
            print(f"warnings : {colors.yellow}{build.warnings_count}{colors.reset}")
        print(f"kernel   : {build.kernel_image_name}")

    return 0


def handle_list(options, config):
    ret = get(config, f"/v1/groups/{config.group}/projects/{config.project}/builds")
    if ret.status_code != 200:
        raise NotImplementedError()

    builds = [Build.new(**b) for b in ret.json()["results"][: options.limit]]
    if options.json:
        print(json.dumps([b.as_dict() for b in builds]))
    else:
        previous_pt = None
        for build in builds:
            state = build.result if build.state == "finished" else build.state
            state_msg = (
                f"{colors.state(build.state, build.result)}{state}{colors.reset}"
            )
            warnings = (
                f" {colors.yellow}warnings={build.warnings_count}{colors.reset}"
                if build.warnings_count
                else ""
            )
            errors = (
                f" {colors.red}errors={build.errors_count}{colors.reset}"
                if build.errors_count
                else ""
            )
            pt = build.provisioning_time
            if pt is None:
                pt = "....-..-..T..:..:........."
            pt = pt[:-7]
            print(
                f"{datediff(previous_pt, pt)} {build.uid} [{state_msg}] {build.target_arch}@{build.toolchain}{errors}{warnings}"
            )

            previous_pt = pt

    return 0


def handle_logs(options, config):
    ret = get(
        config,
        f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
    )
    if ret.status_code != 200:
        raise NotImplementedError()

    # TODO: add a wrapper around requests.get
    ret = requests.get(f"{ret.json()['download_url']}build.log")
    if ret.status_code != 200:
        raise NotImplementedError()
    print(ret.text)
    return 0


def handle_metadata(options, config):
    ret = get(
        config,
        f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
    )
    if ret.status_code != 200:
        raise NotImplementedError()

    # TODO: add a wrapper around requests.get
    ret = requests.get(f"{ret.json()['download_url']}metadata.json")
    if ret.status_code != 200:
        raise NotImplementedError()
    print(ret.text)
    return 0


def handle_open(options, config):
    subprocess.call(
        [
            "xdg-open",
            apiurl(
                config,
                f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
            ),
        ]
    )
    return 0


def handle_wait(options, config):
    previous_state = None
    while True:
        ret = get(
            config,
            f"/v1/groups/{config.group}/projects/{config.project}/builds/{options.uid}",
        )
        if ret.status_code != 200:
            raise NotImplementedError()

        build = Build.new(**ret.json())
        if previous_state is None:
            previous_state = build.state
            print(f"url      : {apiurl(config, build.url())}")
            print(f"download : {build.download_url}")
            print(f"project  : {build.project}")
            print(f"uid      : {build.uid}")
            print(f"plan     : {build.plan}")
            if build.waited_by:
                print(f"tests    : {', '.join(build.waited_by)}")
            print(f"user     : {build.user}")

            print(f"kconfig  : {', '.join(build.kconfig)}")
            print(f"target   : {build.target_arch}@{build.toolchain}")
            print(f"git repo : {build.git_repo}")
            print(f"git ref  : {build.git_ref}")
            print(f"git sha  : {build.git_sha}")
            print(f"git desc : {build.git_describe}")

            if build.provisioning_time:
                print(f"{icons.PROVISIONING} time  : {build.provisioning_time}")
            if build.running_time:
                print(f"{icons.RUNNING} time  : {build.running_time}")

        if build.state != previous_state:
            if build.state == "provisioning":
                print(f"{icons.PROVISIONING} time  : {build.provisioning_time}")
            elif build.state == "running":
                print(f"{icons.RUNNING} time  : {build.running_time}")
            previous_state = build.state
        if build.state == "finished":
            break
        time.sleep(5)

    if build.result == "pass" and build.warnings_count == 0:
        icon = icons.PASS
    elif build.result == "pass" and build.warnings_count != 0:
        icon = icons.WARNING
    elif build.result == "error":
        icon = icons.ERROR
    elif build.result == "fail":
        icon = icons.FAIL
    print(f"{icon} time  : {build.finished_time}")
    if build.duration:
        print(f"duration : {build.duration}")

    print(f"state    : {build.state}")
    if build.result == "pass":
        color = colors.green
    elif build.result in ["error", "fail"]:
        color = colors.red
    print(f"result   : {color}{build.result}{colors.reset}")

    if build.errors_count:
        print(f"warnings : {colors.red}{build.errors_count}{colors.reset}")
    if build.warnings_count:
        print(f"warnings : {colors.yellow}{build.warnings_count}{colors.reset}")

    return 0


handlers = {
    "config": handle_config,
    "download": handle_download,
    "get": handle_get,
    "list": handle_list,
    "logs": handle_logs,
    "metadata": handle_metadata,
    "open": handle_open,
    "wait": handle_wait,
}


def setup_parser(parser):
    # "build config <uid>"
    t = parser.add_parser("config")
    t.add_argument("uid")

    # "build download <uid>"
    t = parser.add_parser("download")
    t.add_argument("--only", default=None, nargs="+")
    t.add_argument("uid")
    t.add_argument("output", type=Path, default=".", nargs="?")

    # "build get <uid>"
    p = parser.add_parser("get")
    p.add_argument("uid")
    p.add_argument("--json", action="store_true")

    # "build list"
    p = parser.add_parser("list")
    p.add_argument("--json", action="store_true")
    p.add_argument("--limit", type=int, default=30)

    # "build logs <uid>"
    t = parser.add_parser("logs")
    t.add_argument("uid")

    # "build metadata <uid>"
    t = parser.add_parser("metadata")
    t.add_argument("uid")

    # "build open <uid>"
    t = parser.add_parser("open")
    t.add_argument("uid")

    # "build wait <uid>"
    p = parser.add_parser("wait")
    p.add_argument("uid")
