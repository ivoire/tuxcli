from tuxcli.argparse import setup_parser
from tuxcli.config import load_config

from tuxcli.build import handlers as build_handlers
from tuxcli.group import handlers as group_handlers
from tuxcli.plan import handlers as plan_handlers
from tuxcli.project import handlers as project_handlers
from tuxcli.test import handlers as test_handlers


def main():
    config = load_config()
    options = setup_parser(config.group, config.project).parse_args()

    config.group = options.group
    config.project = options.project

    if options.sub_command == "build":
        return build_handlers[options.sub_sub_command](options, config)
    elif options.sub_command == "group":
        return group_handlers[options.sub_sub_command](options, config)
    elif options.sub_command == "plan":
        return plan_handlers[options.sub_sub_command](options, config)
    elif options.sub_command == "project":
        return project_handlers[options.sub_sub_command](options, config)
    elif options.sub_command == "test":
        return test_handlers[options.sub_sub_command](options, config)
    raise NotImplementedError()

    return 0
